package br.com.zup.JTABeneficio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class ControleAdivisor {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro manipularExceptionsDeValidacao(MethodArgumentNotValidException exception){
        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();
        List<Erro> erros = fieldErrors.stream().map(objeto -> new Erro(objeto.getDefaultMessage())).collect(Collectors.toList());

        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ClienteNaoEncontradoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro clienteNaoEncontradoException(ClienteNaoEncontradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(CartaoNaoEncontradoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro cartaoNaoEncontradoException(CartaoNaoEncontradoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(BeneficioNaoEncontradoExeception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro beneficioNaoEncontradoExeception(BeneficioNaoEncontradoExeception exeception){
        List<Erro> erros = Arrays.asList(new Erro(exeception.getMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(SaldoInsuficienteException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro saldoInsuficienteException(SaldoInsuficienteException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(CartaoDuplicadoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro cartaoDuplicadoException(CartaoDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ClienteDuplicadoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro clienteDuplicadoException(ClienteDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(EstabelecimentoPesquisaException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro estabelecimentoPesquisaException(EstabelecimentoPesquisaException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(ClientePesquisaPorCpfException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro clientePesquisaPorCpfException(ClienteDuplicadoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(CartaoPesquisaPeloNumeroException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro cartaoPesquisaPeloNumeroException(CartaoPesquisaPeloNumeroException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(BeneficioPesquisaPorIdException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro beneficioPesquisaPorIdException(BeneficioPesquisaPorIdException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(AccessoNegadoException.class)
    public ResponseEntity<?> runtimeHandler(AccessoNegadoException exception){
        HashMap<String, String> mensagem = new HashMap<>();
        mensagem.put("mensagemErro", exception.getMessage());

        return ResponseEntity.status(exception.getStatuscode()).body(mensagem);
    }

    @ExceptionHandler(TokenNotValidException.class)
    public ResponseEntity<?> runtimeHandler(TokenNotValidException exception){
        HashMap<String, String> mensagem = new HashMap<>();
        mensagem.put("mensagemErro", exception.getMessage());

        return ResponseEntity.status(exception.getStatusCode()).body(mensagem);
    }

    @ExceptionHandler(TipoBeneficioAndEstabelecimentoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro tipoBeneficioAndEstabelecimentoException(TipoBeneficioAndEstabelecimentoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }

    @ExceptionHandler(CartaoFalhaTransacaoException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public MensagemDeErro cartaoFalhaTransacaoException(CartaoFalhaTransacaoException exception){
        List<Erro> erros = Arrays.asList(new Erro(exception.getLocalizedMessage()));
        return new MensagemDeErro(400, erros);
    }



}
