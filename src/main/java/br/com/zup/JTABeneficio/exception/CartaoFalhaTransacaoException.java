package br.com.zup.JTABeneficio.exception;

public class CartaoFalhaTransacaoException extends RuntimeException{

    public CartaoFalhaTransacaoException(String message){
        super(message);
    }

}
