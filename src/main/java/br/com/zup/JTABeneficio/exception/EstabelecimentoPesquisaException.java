package br.com.zup.JTABeneficio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EstabelecimentoPesquisaException extends RuntimeException {
    public EstabelecimentoPesquisaException(String message){
        super(message);
    }

}
