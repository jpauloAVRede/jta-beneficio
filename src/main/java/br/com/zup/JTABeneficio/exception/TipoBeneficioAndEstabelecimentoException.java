package br.com.zup.JTABeneficio.exception;

public class TipoBeneficioAndEstabelecimentoException extends RuntimeException{

    public TipoBeneficioAndEstabelecimentoException(String message){
        super(message);
    }
}
