package br.com.zup.JTABeneficio.exception;

public class AccessoNegadoException extends RuntimeException{
    private int statuscode = 403;

    public AccessoNegadoException(){
        super("Acesso negado");
    }

    public int getStatuscode() {
        return statuscode;
    }

    public void setStatuscode(int statuscode) {
        this.statuscode = statuscode;
    }
}
