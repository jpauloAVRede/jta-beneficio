package br.com.zup.JTABeneficio.estabelecimento;

import org.springframework.data.repository.CrudRepository;

public interface EstabelecimentoRepository extends CrudRepository<Estabelecimento, String> {

}
