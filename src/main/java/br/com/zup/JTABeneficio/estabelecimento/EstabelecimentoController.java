package br.com.zup.JTABeneficio.estabelecimento;

import br.com.zup.JTABeneficio.components.Conversor;
import br.com.zup.JTABeneficio.estabelecimento.dtos.entrada.CadastrarEstabelecimentoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/estabelecimento")
public class EstabelecimentoController {
    @Autowired
    private EstabelecimentoService estabelecimentoService;
    @Autowired
    private Conversor conversor;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    private void cadastrarEstabelecimento(@RequestBody @Valid CadastrarEstabelecimentoDTO cadastrarDTO){
        Estabelecimento estabelecimentoModel = this.conversor.modelMapper().map(cadastrarDTO, Estabelecimento.class);
        this.estabelecimentoService.cadastrarEstabelecimento(estabelecimentoModel);
    }

}
