package br.com.zup.JTABeneficio.estabelecimento;

import br.com.zup.JTABeneficio.enums.TipoDeEstabelecimento;

import javax.persistence.*;

@Entity
@Table(name = "estabelecimentos")
public class Estabelecimento {
    @Id
    @Column(nullable = false)
    private String cnpj;
    @Column(nullable = false)
    private String nome;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoDeEstabelecimento tipoDeEstabelecimento;
    @Column(nullable = false)
    private Boolean gerarCashBack;

    public Estabelecimento() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoDeEstabelecimento getTipoDeEstabelecimento() {
        return tipoDeEstabelecimento;
    }

    public void setTipoDeEstabelecimento(TipoDeEstabelecimento tipoDeEstabelecimento) {
        this.tipoDeEstabelecimento = tipoDeEstabelecimento;
    }

    public Boolean getGerarCashBack() {
        return gerarCashBack;
    }

    public void setGerarCashBack(Boolean gerarCashBack) {
        this.gerarCashBack = gerarCashBack;
    }
}
