package br.com.zup.JTABeneficio.estabelecimento;

import br.com.zup.JTABeneficio.exception.ClienteDuplicadoException;
import br.com.zup.JTABeneficio.exception.EstabelecimentoPesquisaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EstabelecimentoService {
    @Autowired
    private EstabelecimentoRepository estabelecimentoRepository;

    public void cadastrarEstabelecimento(Estabelecimento estabelecimento){
        verificaEstabelecimentoDuplicado(estabelecimento.getCnpj());
        this.estabelecimentoRepository.save(estabelecimento);
    }

    public Estabelecimento pesquisaEstabelecimentoPorCNPJ(String cnpj){
        Optional<Estabelecimento> optionalEstabelecimento = this.estabelecimentoRepository.findById(cnpj);
        optionalEstabelecimento.orElseThrow(() -> new EstabelecimentoPesquisaException("Estabelecimento não cadastrado."));
        return optionalEstabelecimento.get();
    }

    public void verificaEstabelecimentoDuplicado(String cnpj){
        if (this.estabelecimentoRepository.existsById(cnpj)){
            throw new ClienteDuplicadoException("Estabelecimento já cadastrado");
        }
    }

    public void deletarEstabelecimento(String cnpj){
        Estabelecimento estabelecimento = pesquisaEstabelecimentoPorCNPJ(cnpj);
        this.estabelecimentoRepository.delete(estabelecimento);
    }

    public void ativarCashBackEstabelecimento(String cnpj){
        Estabelecimento estabelecimento = pesquisaEstabelecimentoPorCNPJ(cnpj);
        estabelecimento.setGerarCashBack(true);
        this.estabelecimentoRepository.save(estabelecimento);
    }

    public void desativarCashBackEstabelecimento(String cnpj){
        Estabelecimento estabelecimento = pesquisaEstabelecimentoPorCNPJ(cnpj);
        estabelecimento.setGerarCashBack(false);
        this.estabelecimentoRepository.save(estabelecimento);
    }

}
