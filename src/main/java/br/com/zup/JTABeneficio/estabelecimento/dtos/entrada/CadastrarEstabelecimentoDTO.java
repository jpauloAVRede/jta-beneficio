package br.com.zup.JTABeneficio.estabelecimento.dtos.entrada;

import br.com.zup.JTABeneficio.enums.TipoDeEstabelecimento;
import org.hibernate.validator.constraints.br.CNPJ;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CadastrarEstabelecimentoDTO {
    @CNPJ(message = "{validacao.cnpj}")
    private String cnpj;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    @Size(min = 3, max = 100, message = "{validacao.tamanho.campo}")
    private String nome;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private TipoDeEstabelecimento tipoDeEstabelecimento;
    private Boolean gerarCashBack;

    public CadastrarEstabelecimentoDTO() {
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoDeEstabelecimento getTipoDeEstabelecimento() {
        return tipoDeEstabelecimento;
    }

    public void setTipoDeEstabelecimento(TipoDeEstabelecimento tipoDeEstabelecimento) {
        this.tipoDeEstabelecimento = tipoDeEstabelecimento;
    }

    public Boolean getGerarCashBack() {
        return gerarCashBack;
    }

    public void setGerarCashBack(Boolean gerarCashBack) {
        this.gerarCashBack = gerarCashBack;
    }
}
