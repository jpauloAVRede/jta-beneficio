package br.com.zup.JTABeneficio.transacao;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransacaoRepository extends CrudRepository<Transacao, Integer> {

    List<Transacao> findAllByCartaoNumeroOrderByDataDesc(String numeroCartao);

}
