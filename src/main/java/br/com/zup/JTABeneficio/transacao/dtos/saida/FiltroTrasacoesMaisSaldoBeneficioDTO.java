package br.com.zup.JTABeneficio.transacao.dtos.saida;

import java.util.List;

public class FiltroTrasacoesMaisSaldoBeneficioDTO {
    private double saldoBeneficio;
    private List<PesqTransacoesPorCartaoDTO> listTransacoesPorCartao;

    public FiltroTrasacoesMaisSaldoBeneficioDTO() {
    }

    public FiltroTrasacoesMaisSaldoBeneficioDTO(double saldoBeneficio, List<PesqTransacoesPorCartaoDTO> listTransacoesPorCartao) {
        this.saldoBeneficio = saldoBeneficio;
        this.listTransacoesPorCartao = listTransacoesPorCartao;
    }

    public double getSaldoBeneficio() {
        return saldoBeneficio;
    }

    public void setSaldoBeneficio(double saldoBeneficio) {
        this.saldoBeneficio = saldoBeneficio;
    }

    public List<PesqTransacoesPorCartaoDTO> getListTransacoesPorCartao() {
        return listTransacoesPorCartao;
    }

    public void setListTransacoesPorCartao(List<PesqTransacoesPorCartaoDTO> listTransacoesPorCartao) {
        this.listTransacoesPorCartao = listTransacoesPorCartao;
    }
}
