package br.com.zup.JTABeneficio.transacao;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import br.com.zup.JTABeneficio.cartao.Cartao;
import br.com.zup.JTABeneficio.cartao.CartaoService;
import br.com.zup.JTABeneficio.components.Conversor;
import br.com.zup.JTABeneficio.transacao.dtos.entrada.RealizarTransacaoPorSenhaDTO;
import br.com.zup.JTABeneficio.transacao.dtos.saida.FiltroTrasacoesMaisSaldoBeneficioDTO;
import br.com.zup.JTABeneficio.transacao.dtos.saida.PesqTransacoesPorCartaoDTO;
import br.com.zup.JTABeneficio.transacao.dtos.entrada.RealizarTransacaoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/transacao")
public class TransacaoController {
    @Autowired
    private TransacaoService transacaoService;
    @Autowired
    private Conversor conversor;
    @Autowired
    private BeneficioService beneficioService;
    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void realizarTransacao(@RequestBody @Valid RealizarTransacaoDTO transacaoDTO){
        Transacao transacaoModel = this.conversor.modelMapper().map(transacaoDTO, Transacao.class);
        this.transacaoService.realizarTransacao(transacaoModel,
                transacaoDTO.getNumeroCartao(), transacaoDTO.getCodSeguranca());
    }

    @PostMapping("/maquina")
    @ResponseStatus(HttpStatus.CREATED)
    public void realizarTransacaoPorSenha(@RequestBody @Valid RealizarTransacaoPorSenhaDTO transacaoDTO){
        Transacao transacaoModel = this.conversor.modelMapper().map(transacaoDTO, Transacao.class);
        this.transacaoService.realizarTansacaoPorSenha(transacaoModel,
                transacaoDTO.getNumeroCartao(), transacaoDTO.getCodSeguranca(), transacaoDTO.getSenhaCartao());
    }

    @GetMapping("/{numeroBeneficio}")
    @ResponseStatus(HttpStatus.OK)
    public FiltroTrasacoesMaisSaldoBeneficioDTO pesquisaTransacaoPorNumeroCartao(@PathVariable String numeroBeneficio, Authentication authentication){
        Beneficio beneficio = this.beneficioService.pesquisaBeneficioPeloNumeroECpfCliente(numeroBeneficio, authentication.getName());
        Cartao cartao = this.cartaoService.pesquisarCartaoPeloBeneficioId(beneficio.getId());
        List<Transacao> transacaoList = this.transacaoService.pesquisaTransacaoPorCartao(cartao.getNumero());
        List<PesqTransacoesPorCartaoDTO> listTransacaoDTO = transacaoList.stream()
                .map(transacao -> this.conversor.modelMapper().map(transacao, PesqTransacoesPorCartaoDTO.class))
                .collect(Collectors.toList());
        FiltroTrasacoesMaisSaldoBeneficioDTO extratoBeneficioDTO = new FiltroTrasacoesMaisSaldoBeneficioDTO(beneficio.getSaldo(), listTransacaoDTO);
        return extratoBeneficioDTO;
    }

}
