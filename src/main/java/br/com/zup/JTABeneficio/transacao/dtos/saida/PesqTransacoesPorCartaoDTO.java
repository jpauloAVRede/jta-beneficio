package br.com.zup.JTABeneficio.transacao.dtos.saida;

import java.time.LocalDate;

public class PesqTransacoesPorCartaoDTO {
    private int id;
    private double valor;
    private LocalDate data;
    private String cnpj;

    public PesqTransacoesPorCartaoDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

}
