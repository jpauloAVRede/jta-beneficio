package br.com.zup.JTABeneficio.transacao.components;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.enums.TipoDeBeneficio;
import br.com.zup.JTABeneficio.enums.TipoDeEstabelecimento;
import br.com.zup.JTABeneficio.estabelecimento.Estabelecimento;
import br.com.zup.JTABeneficio.exception.CartaoFalhaTransacaoException;
import br.com.zup.JTABeneficio.exception.TipoBeneficioAndEstabelecimentoException;
import org.springframework.stereotype.Component;

@Component
public class VerificacaoTransacao {

    public boolean verificaTipoBeneficioAndEstabelecimento(Beneficio beneficio, Estabelecimento estabelecimento){
        if (beneficio.getTipoBeneficio().equals(TipoDeBeneficio.VA)){
            if (estabelecimento.getTipoDeEstabelecimento().equals(TipoDeEstabelecimento.ALIMENTICIO)){
                return true;
            }else {
                throw new TipoBeneficioAndEstabelecimentoException("Modalidade do estabelecimento é imcompatível com o VA");
            }
        }else if (beneficio.getTipoBeneficio().equals(TipoDeBeneficio.VR)){
            if (estabelecimento.getTipoDeEstabelecimento().equals(TipoDeEstabelecimento.RESTAURANTE)){
                return true;
            }else {
                throw new TipoBeneficioAndEstabelecimentoException("Modalidade do estabelecimento é imcompatível com o VR");
            }
        }else if (beneficio.getTipoBeneficio().equals(TipoDeBeneficio.VC)){
            if (estabelecimento.getTipoDeEstabelecimento().equals(TipoDeEstabelecimento.POSTO_COMBUSTIVEL)){
                return true;
            }else {
                throw new TipoBeneficioAndEstabelecimentoException("Modalidade do estabelecimento é imcompatível com o VC");
            }
        }
        throw new TipoBeneficioAndEstabelecimentoException("Empresa não credenciada");
    }

    public boolean verificaCodSeguranca(int codSegurancaRequisicao, int codSegurancaCartao){
        if (codSegurancaCartao != codSegurancaRequisicao){
            throw new CartaoFalhaTransacaoException("ERRO-Falha na transação");
        }
        return true;
    }

    public boolean verificaSenhaCartao(int senhaRequisicao, int senhaCartao){
        if (senhaRequisicao != senhaCartao){
            throw new CartaoFalhaTransacaoException("Erro-Senha Errada");
        }
        return true;
    }

}
