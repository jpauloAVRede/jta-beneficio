package br.com.zup.JTABeneficio.transacao;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.cartao.Cartao;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import br.com.zup.JTABeneficio.cartao.CartaoService;
import br.com.zup.JTABeneficio.cashback.CashBackService;
import br.com.zup.JTABeneficio.estabelecimento.Estabelecimento;
import br.com.zup.JTABeneficio.estabelecimento.EstabelecimentoService;
import br.com.zup.JTABeneficio.transacao.components.VerificacaoTransacao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class TransacaoService {
    @Autowired
    private TransacaoRepository transacaoRepository;
    @Autowired
    private BeneficioService beneficioService;
    @Autowired
    private CartaoService cartaoService;
    @Autowired
    private EstabelecimentoService estabelecimentoService;
    @Autowired
    private VerificacaoTransacao verificacaoTransacao;
    @Autowired
    private CashBackService cashBackService;

    public void realizarTransacao(Transacao transacao, String numeroCartao, int codSegurancaRequisicao){
        Cartao cartao = this.cartaoService.pesquisarCartaoPeloNumero(numeroCartao);
        Beneficio beneficio = this.beneficioService.pesquisarBeneficioPorID(cartao.getBeneficio().getId());
        Estabelecimento estabelecimento = this.estabelecimentoService.pesquisaEstabelecimentoPorCNPJ(transacao.getCnpjEmpresa());

        this.verificacaoTransacao.verificaTipoBeneficioAndEstabelecimento(beneficio, estabelecimento);
        this.verificacaoTransacao.verificaCodSeguranca(codSegurancaRequisicao, cartao.getCodSeguranca());

        if (estabelecimento.getGerarCashBack()) {
            this.cashBackService.adicionarSaldoNoCashBack(transacao.getValor(), beneficio);
        }

        this.beneficioService.debitarDoSaldo(cartao.getBeneficio().getId(), transacao.getValor());
        transacao.setData(LocalDate.now());

        this.transacaoRepository.save(transacao);
    }

    public void realizarTansacaoPorSenha(Transacao transacao, String numeroCartao, int codSegurancaRequisicao, int senhaCartaoRequisicao){
        Cartao cartao = this.cartaoService.pesquisarCartaoPeloNumero(numeroCartao);
        Beneficio beneficio = this.beneficioService.pesquisarBeneficioPorID(cartao.getBeneficio().getId());
        Estabelecimento estabelecimento = this.estabelecimentoService.pesquisaEstabelecimentoPorCNPJ(transacao.getCnpjEmpresa());

        this.verificacaoTransacao.verificaTipoBeneficioAndEstabelecimento(beneficio, estabelecimento);
        this.verificacaoTransacao.verificaCodSeguranca(codSegurancaRequisicao, cartao.getCodSeguranca());
        this.verificacaoTransacao.verificaSenhaCartao(senhaCartaoRequisicao, cartao.getSenhaCartao());

        if (estabelecimento.getGerarCashBack()) {
            this.cashBackService.adicionarSaldoNoCashBack(transacao.getValor(), beneficio);
        }

        this.beneficioService.debitarDoSaldo(cartao.getBeneficio().getId(), transacao.getValor());
        transacao.setData(LocalDate.now());

        this.transacaoRepository.save(transacao);
    }

    public List<Transacao> pesquisaTransacaoPorCartao(String numeroCartao){
        return this.transacaoRepository.findAllByCartaoNumeroOrderByDataDesc(numeroCartao);
    }

}
