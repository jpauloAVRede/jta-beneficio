package br.com.zup.JTABeneficio.transacao.dtos.entrada;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class RealizarTransacaoPorSenhaDTO {
    @Min(value = 1, message = "{validacao.valor.transacao}")
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private double valor;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    private String cnpjEmpresa;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    @Size(min = 16, max = 16, message = "{validacao.tamanho.campo}")
    private String numeroCartao;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @Min(value = 100, message = "{valicadao.codSegurança}")
    @Max(value = 999, message = "{valicadao.codSegurança}")
    private Integer codSeguranca;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @Min(value = 1000, message = "{valicadao.senhaCartao}")
    @Max(value = 9999, message = "{valicadao.senhaCartao}")
    private Integer senhaCartao;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private LocalDate dataExpiracao;

    public RealizarTransacaoPorSenhaDTO() {
    }

    public double getValor() {
            return valor;
        }

    public void setValor(double valor) {
            this.valor = valor;
        }

    public String getCnpjEmpresa() {
            return cnpjEmpresa;
        }

    public void setCnpjEmpresa(String cnpjEmpresa) {
            this.cnpjEmpresa = cnpjEmpresa;
        }

    public String getNumeroCartao() {
            return numeroCartao;
        }

    public void setNumeroCartao(String numeroCartao) {
            this.numeroCartao = numeroCartao;
        }

    public Integer getCodSeguranca() {
            return codSeguranca;
        }

    public void setCodSeguranca(Integer codSeguranca) {
            this.codSeguranca = codSeguranca;
        }

    public LocalDate getDataExpiracao() {
            return dataExpiracao;
        }

    public void setDataExpiracao(LocalDate dataExpiracao) {
            this.dataExpiracao = dataExpiracao;
    }

    public int getSenhaCartao() {
        return senhaCartao;
    }

    public void setSenhaCartao(int senhaCartao) {
        this.senhaCartao = senhaCartao;
    }
}
