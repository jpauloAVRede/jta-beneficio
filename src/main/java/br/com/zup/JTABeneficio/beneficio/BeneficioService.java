package br.com.zup.JTABeneficio.beneficio;

import br.com.zup.JTABeneficio.exception.BeneficioNaoEncontradoExeception;
import br.com.zup.JTABeneficio.exception.SaldoInsuficienteException;
import br.com.zup.JTABeneficio.cliente.Cliente;
import br.com.zup.JTABeneficio.cliente.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BeneficioService {
    @Autowired
    private BeneficioRepository beneficioRepository;
    @Autowired
    private ClienteService clienteService;

    public void criarBeneficio(Beneficio beneficio, String cpfCliente){
        Cliente cliente = this.clienteService.pesquisarClientePorCPF(cpfCliente);
        beneficio.setCliente(cliente);
        this.beneficioRepository.save(beneficio);
    }

    public Beneficio pesquisarBeneficioPorID(int idBeneficio){
        Optional<Beneficio> optionalBeneficio = this.beneficioRepository.findById(idBeneficio);
        optionalBeneficio.orElseThrow(() -> new BeneficioNaoEncontradoExeception("Benefício não encontrado"));

        return optionalBeneficio.get();
    }

    public void debitarDoSaldo(int idBeneficio, double valorDebito){
        Beneficio beneficioDebitar = pesquisarBeneficioPorID(idBeneficio);
        if (beneficioDebitar.getSaldo() < valorDebito){
            throw new SaldoInsuficienteException("Saldo Insuficiente");
        }
        double novoSaldo = (beneficioDebitar.getSaldo() - valorDebito);
        beneficioDebitar.setSaldo(novoSaldo);
    }

    public void creditarNoSaldo(int idBeneficio, double valorCredito){
        Beneficio beneficioCreditar = pesquisarBeneficioPorID(idBeneficio);
        double novoSaldo = (beneficioCreditar.getSaldo() + valorCredito);
        beneficioCreditar.setSaldo(novoSaldo);
    }

    public Beneficio pesquisaBeneficioPeloNumeroECpfCliente(String numBeneficio, String cpf){
        Optional<Beneficio> optionalBeneficio = this.beneficioRepository.findByNumeroBeneficioAndClienteCpf(numBeneficio, cpf);
        optionalBeneficio.orElseThrow(() -> new RuntimeException("Beneficio não pertence ao usuário."));
        return optionalBeneficio.get();
    }

    public List<Beneficio> pesquisaBeneficiosPeloCpf(String numeroCPF){
        return this.beneficioRepository.findAllByClienteCpf(numeroCPF);
    }

}
