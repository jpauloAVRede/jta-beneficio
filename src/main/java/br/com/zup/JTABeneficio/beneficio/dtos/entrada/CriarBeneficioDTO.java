package br.com.zup.JTABeneficio.beneficio.dtos.entrada;

import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.*;

public class CriarBeneficioDTO {
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    @Size(min = 3, max = 6, message = "{validacao.tamanho.campo}")
    private String numeroBeneficio;
    @Min(value = 0)
    private double saldo;
    @CPF(message = "{validacao.cpf.invalido}")
    private String cpf;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    private String tipoBeneficio;

    public CriarBeneficioDTO() {
    }

    public String getNumeroBeneficio() {
        return numeroBeneficio;
    }

    public void setNumeroBeneficio(String numeroBeneficio) {
        this.numeroBeneficio = numeroBeneficio;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(String tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
