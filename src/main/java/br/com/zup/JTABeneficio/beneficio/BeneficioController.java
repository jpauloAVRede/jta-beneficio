package br.com.zup.JTABeneficio.beneficio;

import br.com.zup.JTABeneficio.beneficio.dtos.entrada.CriarBeneficioDTO;
import br.com.zup.JTABeneficio.beneficio.dtos.saida.PesquisaBeneficiosPeloCpfDTO;
import br.com.zup.JTABeneficio.cartao.Cartao;
import br.com.zup.JTABeneficio.components.Conversor;
import br.com.zup.JTABeneficio.transacao.Transacao;
import br.com.zup.JTABeneficio.transacao.dtos.saida.FiltroTrasacoesMaisSaldoBeneficioDTO;
import br.com.zup.JTABeneficio.transacao.dtos.saida.PesqTransacoesPorCartaoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/beneficio")
public class BeneficioController {
    @Autowired
    private BeneficioService beneficioService;
    @Autowired
    private Conversor conversor;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    private void criarBeneficio(@RequestBody CriarBeneficioDTO beneficioDTO){
        Beneficio beneficioModel = this.conversor.modelMapper().map(beneficioDTO, Beneficio.class);
        this.beneficioService.criarBeneficio(beneficioModel, beneficioDTO.getCpf());
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<PesquisaBeneficiosPeloCpfDTO> pesquisaBeneficiosPeloCpf(Authentication authentication){
        List<Beneficio> beneficioList = this.beneficioService.pesquisaBeneficiosPeloCpf(authentication.getName());
        List<PesquisaBeneficiosPeloCpfDTO> beneficiosPeloCpfDTOList = beneficioList.stream()
                .map(beneficio -> this.conversor.modelMapper().map(beneficio, PesquisaBeneficiosPeloCpfDTO.class))
                .collect(Collectors.toList());
        return beneficiosPeloCpfDTOList;
    }

}
