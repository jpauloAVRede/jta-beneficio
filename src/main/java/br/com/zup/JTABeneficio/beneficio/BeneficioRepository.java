package br.com.zup.JTABeneficio.beneficio;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BeneficioRepository extends CrudRepository<Beneficio, Integer> {

    Optional<Beneficio> findByNumeroBeneficioAndClienteCpf(String numBeneficio, String cpf);

    List<Beneficio> findAllByClienteCpf(String cpf);
}
