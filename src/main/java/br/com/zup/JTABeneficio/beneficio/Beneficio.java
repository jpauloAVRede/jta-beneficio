package br.com.zup.JTABeneficio.beneficio;

import br.com.zup.JTABeneficio.cliente.Cliente;
import br.com.zup.JTABeneficio.enums.TipoDeBeneficio;

import javax.persistence.*;

@Entity
@Table(name = "beneficios")
public class Beneficio {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String numeroBeneficio;
    private double saldo;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private TipoDeBeneficio tipoBeneficio;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Cliente cliente;

    public Beneficio() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumeroBeneficio() {
        return numeroBeneficio;
    }

    public void setNumeroBeneficio(String numeroBeneficio) {
        this.numeroBeneficio = numeroBeneficio;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public TipoDeBeneficio getTipoBeneficio() {
        return tipoBeneficio;
    }

    public void setTipoBeneficio(TipoDeBeneficio tipoBeneficio) {
        this.tipoBeneficio = tipoBeneficio;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}

