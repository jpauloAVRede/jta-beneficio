package br.com.zup.JTABeneficio.beneficio.dtos.saida;

import br.com.zup.JTABeneficio.enums.TipoDeBeneficio;

public class PesquisaBeneficiosPeloCpfDTO {
    private String numeroBeneficio;
    private double saldo;
    private TipoDeBeneficio tipoDeBeneficio;

    public PesquisaBeneficiosPeloCpfDTO() {
    }

    public String getNumeroBeneficio() {
        return numeroBeneficio;
    }

    public void setNumeroBeneficio(String numeroBeneficio) {
        this.numeroBeneficio = numeroBeneficio;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public TipoDeBeneficio getTipoDeBeneficio() {
        return tipoDeBeneficio;
    }

    public void setTipoDeBeneficio(TipoDeBeneficio tipoDeBeneficio) {
        this.tipoDeBeneficio = tipoDeBeneficio;
    }
}
