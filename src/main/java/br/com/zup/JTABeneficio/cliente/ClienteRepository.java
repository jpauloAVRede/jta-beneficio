package br.com.zup.JTABeneficio.cliente;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ClienteRepository extends CrudRepository<Cliente, String> {

    Cliente findByCpf(String cpf);

    Optional<Cliente> findByEmail(String email);

}
