package br.com.zup.JTABeneficio.cliente;

import br.com.zup.JTABeneficio.exception.ClienteDuplicadoException;
import br.com.zup.JTABeneficio.exception.ClienteNaoEncontradoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {
    @Autowired
    private ClienteRepository clienteRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public void cadastrarCliente(Cliente cliente){
        verificaClienteDuplicado(cliente.getCpf());
        String encode = this.bCryptPasswordEncoder.encode(cliente.getSenha());
        cliente.setSenha(encode);
        this.clienteRepository.save(cliente);
    }

    public Cliente pesquisarClientePorCPF(String idCPF){
        Optional<Cliente> clienteOptional = this.clienteRepository.findById(idCPF);
        clienteOptional.orElseThrow(() -> new ClienteNaoEncontradoException("Cliente não cadastrado"));

        return clienteOptional.get();
    }

    public void verificaClienteDuplicado(String cpfCliente){
        if (this.clienteRepository.existsById(cpfCliente)){
            throw new ClienteDuplicadoException("Cliente já cadastrado");
        }
    }

}
