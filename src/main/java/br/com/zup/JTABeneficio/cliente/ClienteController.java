package br.com.zup.JTABeneficio.cliente;

import br.com.zup.JTABeneficio.cliente.dtos.CadastrarClienteDTO;
import br.com.zup.JTABeneficio.components.Conversor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private Conversor conversor;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarCliente(@RequestBody @Valid CadastrarClienteDTO clienteDTO){
        Cliente clienteModel = this.conversor.modelMapper().map(clienteDTO, Cliente.class);
        this.clienteService.cadastrarCliente(clienteModel);
    }


}
