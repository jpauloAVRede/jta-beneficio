package br.com.zup.JTABeneficio.cliente.dtos;

import br.com.zup.JTABeneficio.enums.RolesEnum;
import org.hibernate.validator.constraints.br.CPF;

import javax.validation.constraints.*;

public class CadastrarClienteDTO {
    @Size(min = 2, max = 20, message = "{validacao.tamanho.campo}")
    private String nome;
    @Size(min = 2, max = 20, message = "{validacao.tamanho.campo}")
    private String sobrenome;
    @CPF(message = "{validacao.cpf.invalido}")
    private String cpf;
    @Email(message = "{validacao.email}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    private String email;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    @Size(min = 6, max = 16, message = "{validacao.tamanho.campo}")
    private String senha;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private RolesEnum nivelDeAcesso;

    public CadastrarClienteDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public RolesEnum getNivelDeAcesso() {
        return nivelDeAcesso;
    }

    public void setNivelDeAcesso(RolesEnum nivelDeAcesso) {
        this.nivelDeAcesso = nivelDeAcesso;
    }

}
