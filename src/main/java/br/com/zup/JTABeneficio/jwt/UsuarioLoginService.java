package br.com.zup.JTABeneficio.jwt;

import br.com.zup.JTABeneficio.cliente.Cliente;
import br.com.zup.JTABeneficio.cliente.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioLoginService implements UserDetailsService {
    @Autowired
    private ClienteRepository clienteRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Cliente> clienteOptional = this.clienteRepository.findById(username);
        clienteOptional.orElseThrow(() -> new UsernameNotFoundException("Usuario não encontrado"));
        Cliente cliente = clienteOptional.get();

        return new UsuarioLogin(cliente.getCpf(), cliente.getEmail(), cliente.getSenha(),
                cliente.getNivelDeAcesso());
    }
}
