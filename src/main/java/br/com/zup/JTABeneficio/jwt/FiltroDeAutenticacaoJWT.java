package br.com.zup.JTABeneficio.jwt;

import br.com.zup.JTABeneficio.cliente.Cliente;
import br.com.zup.JTABeneficio.cliente.ClienteService;
import br.com.zup.JTABeneficio.jwt.dtos.LoginDTO;
import br.com.zup.JTABeneficio.exception.AccessoNegadoException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class FiltroDeAutenticacaoJWT extends UsernamePasswordAuthenticationFilter {
    private JWTComponente jwtComponente;
    private AuthenticationManager authenticationManager;
    private ClienteService clienteService;

    public FiltroDeAutenticacaoJWT(AuthenticationManager authenticationManager, JWTComponente jwtComponente, ClienteService clienteService) {
        this.jwtComponente = jwtComponente;
        this.authenticationManager = authenticationManager;
        this.clienteService = clienteService;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        ObjectMapper objectMapper = new ObjectMapper();

        try{
            LoginDTO login = objectMapper.readValue(request.getInputStream(), LoginDTO.class);
            Cliente clienteLogin = clienteService.pesquisarClientePorCPF(login.getCpf());
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    login.getCpf(), login.getSenha(), Arrays.asList(
                    new SimpleGrantedAuthority(String.valueOf(clienteLogin.getNivelDeAcesso()))
            ));

            Authentication auth = authenticationManager.authenticate(authToken);
            return auth;
        }catch (IOException exception){
            throw new AccessoNegadoException();
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        UsuarioLogin usuarioLogin = (UsuarioLogin) authResult.getPrincipal();
        String username = usuarioLogin.getUsername();
        String idUsuario = usuarioLogin.getCpf();

        String token = jwtComponente.gerarToken(username, idUsuario);

        response.setHeader("Access-Control-Expose-Headers", "Authorization");
        response.addHeader("Authorization", "Token "+token);


    }
}
