package br.com.zup.JTABeneficio.cashback;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import br.com.zup.JTABeneficio.cashback.dtos.PesqCashBackDTO;
import br.com.zup.JTABeneficio.components.Conversor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cashback")
public class CashBackController {
    @Autowired
    private CashBackService cashBackService;
    @Autowired
    private BeneficioService beneficioService;
    @Autowired
    private Conversor conversor;

    @GetMapping("/{numeroBeneficio}")
    public PesqCashBackDTO pesquisaCashBack(@PathVariable String numeroBeneficio, Authentication authentication){
        Beneficio beneficio = this.beneficioService.pesquisaBeneficioPeloNumeroECpfCliente(numeroBeneficio, authentication.getName());
        CashBack cashBackModel = this.cashBackService.pesquisaSeExisteCashBackPeloIdBeneficio(beneficio);
        return this.conversor.modelMapper().map(cashBackModel, PesqCashBackDTO.class);
    }

}
