package br.com.zup.JTABeneficio.cashback;

import org.springframework.data.repository.CrudRepository;

public interface CashBackRepository extends CrudRepository<CashBack, Integer> {

    CashBack findByBeneficioId(int idBeneficio);

}
