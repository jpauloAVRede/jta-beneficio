package br.com.zup.JTABeneficio.cashback;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class CashBackService {
    @Autowired
    private CashBackRepository cashBackRepository;
    @Autowired
    private BeneficioService beneficioService;

    protected CashBack criarCashBack(Beneficio beneficio){
        CashBack novoCashBack = new CashBack();
        novoCashBack.setSaldoCash(0);
        novoCashBack.setBeneficio(beneficio);
        novoCashBack.setDataCredito(LocalDate.now());

        this.cashBackRepository.save(novoCashBack);

        return novoCashBack;
    }

    public CashBack pesquisaSeExisteCashBackPeloIdBeneficio(Beneficio beneficio){
        CashBack cashBack = this.cashBackRepository.findByBeneficioId(beneficio.getId());
        if (cashBack == null){
            return criarCashBack(beneficio);
        }else {
            return cashBack;
        }
    }

    public double adicionarSaldoNoCashBack(double valorTransacao, Beneficio beneficio){
        CashBack cashBack = pesquisaSeExisteCashBackPeloIdBeneficio(beneficio);
        double valorCreditar = 0;

        if (cashBack.getSaldoCash() >= 10){
            cashBack = utilizarSaldoCashNoBeneficio(cashBack);
        }

        if (valorTransacao > 20){
            valorCreditar = calculaValorCashBack(valorTransacao);
            cashBack.setSaldoCash(cashBack.getSaldoCash() + valorCreditar);
            this.cashBackRepository.save(cashBack);
            return valorCreditar;
        }
        return valorCreditar;
    }

    protected double calculaValorCashBack(double valortansacao){
        double totalCaskBack = (valortansacao * 0.05);
        return totalCaskBack;
    }

    public CashBack utilizarSaldoCashNoBeneficio(CashBack cashBack){
        this.beneficioService.creditarNoSaldo(cashBack.getBeneficio().getId(), cashBack.getSaldoCash());
        cashBack.setSaldoCash(0);
        return this.cashBackRepository.save(cashBack);
    }

}
