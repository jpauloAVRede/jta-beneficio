package br.com.zup.JTABeneficio.cashback;

import br.com.zup.JTABeneficio.beneficio.Beneficio;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "cashbacks")
public class CashBack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private double saldoCash;
    @Column(nullable = false)
    private LocalDate dataCredito;
    @OneToOne
    @JoinColumn(unique = true, nullable = false)
    private Beneficio beneficio;

    public CashBack() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSaldoCash() {
        return saldoCash;
    }

    public void setSaldoCash(double saldoCash) {
        this.saldoCash = saldoCash;
    }

    public LocalDate getDataCredito() {
        return dataCredito;
    }

    public void setDataCredito(LocalDate dataCredito) {
        this.dataCredito = dataCredito;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }
}
