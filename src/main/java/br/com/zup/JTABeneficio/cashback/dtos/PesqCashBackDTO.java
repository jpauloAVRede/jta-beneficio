package br.com.zup.JTABeneficio.cashback.dtos;

public class PesqCashBackDTO {
    private int id;
    private double saldo;
    private String beneficio;

    public PesqCashBackDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public String getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(String beneficio) {
        this.beneficio = beneficio;
    }
}
