package br.com.zup.JTABeneficio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtaBeneficioApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtaBeneficioApplication.class, args);
	}

}
