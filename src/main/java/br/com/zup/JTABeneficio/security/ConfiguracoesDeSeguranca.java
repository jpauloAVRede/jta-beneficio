package br.com.zup.JTABeneficio.security;

import br.com.zup.JTABeneficio.cliente.ClienteService;
import br.com.zup.JTABeneficio.enums.RolesEnum;
import br.com.zup.JTABeneficio.jwt.FiltroDeAutenticacaoJWT;
import br.com.zup.JTABeneficio.jwt.FiltroDeAutorizacaoJWT;
import br.com.zup.JTABeneficio.jwt.JWTComponente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracoesDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JWTComponente jwtComponente;
    @Autowired
    private ClienteService clienteService;

    private static final String[] GET_PUBLICOS = {
            "/cliente/{\\d+}",
    };
    private static final String[] POST_PUBLICOS = {
            "/cliente",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors().configurationSource(configuracaoDeCors());

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, POST_PUBLICOS).permitAll()
                .antMatchers(HttpMethod.GET, GET_PUBLICOS).permitAll()
                .antMatchers(HttpMethod.POST, "/estabelecimento/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_ADMIM))
                .antMatchers(HttpMethod.POST, "/beneficio/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_ADMIM))
                .antMatchers(HttpMethod.GET, "/beneficio/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_ADMIM), String.valueOf(RolesEnum.PERFIL_USER))
                .antMatchers(HttpMethod.POST, "/cartao/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_ADMIM))
                .antMatchers(HttpMethod.POST, "/transacao/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_USER))
                .antMatchers(HttpMethod.GET, "/transacao/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_USER))
                .antMatchers(HttpMethod.POST, "/cashback/**").hasAnyAuthority(String.valueOf(RolesEnum.PERFIL_USER))
                .anyRequest().authenticated();
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilter(new FiltroDeAutenticacaoJWT(authenticationManager(), jwtComponente, clienteService));
        http.addFilter(new FiltroDeAutorizacaoJWT(authenticationManager(), jwtComponente, userDetailsService));
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
    }

    @Bean
    CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return source;
    }

}
