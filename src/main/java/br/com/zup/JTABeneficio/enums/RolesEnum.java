package br.com.zup.JTABeneficio.enums;

public enum RolesEnum {

    PERFIL_USER(2, "ROLE_USER", "hasRole('PERFIL_USER')", "Acesso Básico"),
    PERFIL_ADMIM(1,"ROLE_ADMIM", "hasRole('PERFIL_ADMIM')", "Acesso Gerar Beneficio");

    private Integer id;
    private String role;
    private String roleAcess;
    private String descricao;

    RolesEnum(Integer id, String role, String roleAcess, String descricao) {
        this.id = id;
        this.role = role;
        this.roleAcess = roleAcess;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getRoleAcess() {
        return roleAcess;
    }

    public void setRoleAcess(String roleAcess) {
        this.roleAcess = roleAcess;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
