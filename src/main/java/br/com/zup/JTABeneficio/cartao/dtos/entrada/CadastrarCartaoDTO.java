package br.com.zup.JTABeneficio.cartao.dtos.entrada;

import javax.validation.constraints.*;
import java.time.LocalDate;

public class CadastrarCartaoDTO {
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @NotBlank(message = "{validacao.campo.em.branco}")
    @NotEmpty(message = "{validacao.campo.vazio}")
    @Size(min = 16, max = 16, message = "{validacao.tamanho.campo}")
    private String numero;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @Min(value = 100, message = "{valicadao.codSegurança}")
    @Max(value = 999, message = "{valicadao.codSegurança}")
    private int codSeguranca;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    @Min(value = 1000, message = "{valicadao.senhaCartao}")
    @Max(value = 9999, message = "{valicadao.senhaCartao}")
    private int senhaCartao;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private LocalDate dataExpiracao;
    @NotNull(message = "{validacao.campo.obrigatorio}")
    private int beneficio;

    public CadastrarCartaoDTO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCodSeguranca() {
        return codSeguranca;
    }

    public void setCodSeguranca(int codSeguranca) {
        this.codSeguranca = codSeguranca;
    }

    public LocalDate getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(LocalDate dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    public int getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(int beneficio) {
        this.beneficio = beneficio;
    }

    public int getSenhaCartao() {
        return senhaCartao;
    }

    public void setSenhaCartao(int senhaCartao) {
        this.senhaCartao = senhaCartao;
    }
}
