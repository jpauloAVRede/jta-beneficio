package br.com.zup.JTABeneficio.cartao;

import br.com.zup.JTABeneficio.beneficio.Beneficio;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "cartoes")
public class Cartao {
    @Id
    @Column(nullable = false)
    private String numero;
    @Column(nullable = false)
    private int codSeguranca;
    @Column(nullable = false)
    private int senhaCartao;
    @Column(nullable = false)
    private LocalDate dataExpiracao;
    @ManyToOne
    @JoinColumn(nullable = false)
    private Beneficio beneficio;

    public Cartao() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public int getCodSeguranca() {
        return codSeguranca;
    }

    public void setCodSeguranca(int codSeguranca) {
        this.codSeguranca = codSeguranca;
    }

    public LocalDate getDataExpiracao() {
        return dataExpiracao;
    }

    public void setDataExpiracao(LocalDate dataExpiracao) {
        this.dataExpiracao = dataExpiracao;
    }

    public Beneficio getBeneficio() {
        return beneficio;
    }

    public void setBeneficio(Beneficio beneficio) {
        this.beneficio = beneficio;
    }

    public int getSenhaCartao() {
        return senhaCartao;
    }

    public void setSenhaCartao(int senhaCartao) {
        this.senhaCartao = senhaCartao;
    }
}
