package br.com.zup.JTABeneficio.cartao;

import br.com.zup.JTABeneficio.exception.CartaoDuplicadoException;
import br.com.zup.JTABeneficio.exception.CartaoNaoEncontradoException;
import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    private CartaoRepository cartaoRepository;
    @Autowired
    private BeneficioService beneficioService;

    public void cadastrarCartao(Cartao cartao, int idBeneficio){
        verificaCartaoDuplicado(cartao.getNumero());
        Beneficio beneficio = this.beneficioService.pesquisarBeneficioPorID(idBeneficio);
        cartao.setBeneficio(beneficio);
        this.cartaoRepository.save(cartao);
    }

    public Cartao pesquisarCartaoPeloNumero(String numeroCartao){
        Optional<Cartao> optionalCartao = this.cartaoRepository.findById(numeroCartao);
        optionalCartao.orElseThrow(() -> new CartaoNaoEncontradoException("Cartão não existente"));
        return optionalCartao.get();
    }

    public void verificaCartaoDuplicado(String numeroCartao){
        if (this.cartaoRepository.existsById(numeroCartao)){
            throw new CartaoDuplicadoException("Cartão já em uso!!");
        }
    }

    public Cartao pesquisarCartaoPeloBeneficioId(int idBeneficio){
        Optional<Cartao> optionalCartao = this.cartaoRepository.findByBeneficioId(idBeneficio);
        optionalCartao.orElseThrow(() -> new CartaoNaoEncontradoException("Cartão não existente"));
        return optionalCartao.get();
    }

}
