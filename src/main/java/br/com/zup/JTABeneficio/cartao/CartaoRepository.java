package br.com.zup.JTABeneficio.cartao;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, String> {

    Optional<Cartao> findByBeneficioId (int idBeneficio);

}
