package br.com.zup.JTABeneficio.cartao;

import br.com.zup.JTABeneficio.cartao.dtos.entrada.CadastrarCartaoDTO;
import br.com.zup.JTABeneficio.components.Conversor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;
    @Autowired
    private Conversor conversor;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void cadastrarCartao(@RequestBody @Valid CadastrarCartaoDTO cartaoDTO){
        Cartao cartaoModel = this.conversor.modelMapper().map(cartaoDTO, Cartao.class);
        this.cartaoService.cadastrarCartao(cartaoModel, cartaoDTO.getBeneficio());
    }

}
