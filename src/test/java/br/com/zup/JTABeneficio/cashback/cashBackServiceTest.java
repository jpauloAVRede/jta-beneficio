package br.com.zup.JTABeneficio.cashback;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import br.com.zup.JTABeneficio.enums.TipoDeBeneficio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootTest
public class cashBackServiceTest {
    @Autowired
    private CashBackService cashBackService;
    @MockBean
    private CashBackRepository cashBackRepository;
    @MockBean
    private BeneficioService beneficioService;

    private CashBack cashBack;
    private Beneficio beneficio;

    @BeforeEach
    public void setup() {
        this.beneficio = new Beneficio();
        this.beneficio.setId(1);
        this.beneficio.setNumeroBeneficio("111");
        this.beneficio.setSaldo(100.00);
        this.beneficio.setTipoBeneficio(TipoDeBeneficio.VA);
        this.beneficio.setCliente(beneficio.getCliente());

        this.cashBack = new CashBack();
        this.cashBack.setId(1);
        this.cashBack.setBeneficio(beneficio);
        this.cashBack.setSaldoCash(50.00);
        this.cashBack.setDataCredito(LocalDate.EPOCH);
        this.cashBack.setBeneficio(beneficio);
    }

}
