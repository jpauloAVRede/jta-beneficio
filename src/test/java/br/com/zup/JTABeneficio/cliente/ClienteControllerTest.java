package br.com.zup.JTABeneficio.cliente;

import br.com.zup.JTABeneficio.cliente.dtos.CadastrarClienteDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

    @MockBean
    private ClienteService clienteService;

    @Autowired
    private MockMvc mockMvc;

    private CadastrarClienteDTO clienteDTO;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void SetUp (){
        clienteDTO = new CadastrarClienteDTO();
        clienteDTO.setNome("Lucas");
        clienteDTO.setSobrenome("César");
        clienteDTO.setEmail("lucas@123");
        clienteDTO.setCpf("34587698763");
        clienteDTO.setSenha("123");

        objectMapper = new ObjectMapper();
    }

}
