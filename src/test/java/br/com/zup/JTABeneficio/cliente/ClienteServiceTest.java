package br.com.zup.JTABeneficio.cliente;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class ClienteServiceTest {

    @Autowired
    private ClienteService clienteService;
    @MockBean
    private ClienteRepository clienteRepository;

    @Test
    public void testeMetodoCadastrarCliente(){

        Cliente cliente = new Cliente();

        this.clienteService.cadastrarCliente(cliente);
        Mockito.verify(clienteRepository, Mockito.times(1)).save(cliente);
    }

   @Test
    public void testeMetodoPesquisarClientePorCPFPositivo(){
        Cliente cliente = new Cliente();
        Optional<Cliente> opitionalCliente = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(Mockito.anyString())).thenReturn(opitionalCliente);
        Cliente test = clienteService.pesquisarClientePorCPF("23.524.377/0001-45");
        Assertions.assertEquals(test, cliente);
    }

    @Test
    public void testeMetodoPesquisaClientePorCPFNegativo(){
        Cliente cliente = new Cliente();
        Optional<Cliente> optionalCliente = Optional.empty();
        Mockito.when(clienteRepository.findById(Mockito.anyString())).thenReturn(optionalCliente);
        Assertions.assertThrows(RuntimeException.class, () -> {
            clienteService.pesquisarClientePorCPF("23.524.377/0001-45");
        });
    }

    @Test
    public void testeMetodoVerificaClienteDuplicadoCaminhoPositivo(){
        Cliente cliente = new Cliente();
        Mockito.when(this.clienteRepository.existsById(Mockito.anyString())).thenReturn(false);
    }

    @Test
    public void testeMetodoVerificaClienteDuplicadoCaminhoNegativo(){
        Cliente cliente = new Cliente();
        Mockito.when(this.clienteRepository.existsById(Mockito.anyString())).thenReturn(true);
        RuntimeException exception = Assertions
                .assertThrows(RuntimeException.class, () -> {this.clienteService.verificaClienteDuplicado("63739859032");});
        Assertions.assertFalse(exception.getMessage().equals("Cliente não cadastrado"));
    }

}
