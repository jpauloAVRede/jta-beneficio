package br.com.zup.JTABeneficio.beneficio;

import br.com.zup.JTABeneficio.cliente.Cliente;
import br.com.zup.JTABeneficio.cliente.ClienteService;
import br.com.zup.JTABeneficio.enums.TipoDeBeneficio;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class BeneficioServiceTest {

    @Autowired
    private BeneficioService beneficioService;
    @MockBean
    private BeneficioRepository beneficioRepository;
    @MockBean
    private ClienteService clienteService;

    private Beneficio beneficio;
    private Cliente cliente;

    @BeforeEach
    public void setup() {
        this.cliente = new Cliente();
        this.cliente.setCpf("111111111");
        this.cliente.setEmail("email@email.com");
        this.cliente.setNome("José");
        this.cliente.setSobrenome("Vieira");
        this.cliente.setSenha("senha");

        this.beneficio = new Beneficio();
        this.beneficio.setId(1);
        this.beneficio.setNumeroBeneficio("111");
        this.beneficio.setSaldo(100.00);
        this.beneficio.setTipoBeneficio(TipoDeBeneficio.VA);
        this.beneficio.setCliente(cliente);
    }

    @Test
    public void testarMetodoCriarBeneficio(){
        Mockito.when(this.clienteService.pesquisarClientePorCPF(Mockito.anyString())).thenReturn(cliente);
        beneficio.setCliente(cliente);
        Mockito.when(this.beneficioRepository.save(Mockito.any(Beneficio.class))).thenReturn(beneficio);
        this.beneficioService.criarBeneficio(beneficio, cliente.getCpf());
        Mockito.verify(this.beneficioRepository, Mockito.times(1)).save(beneficio);
    }

    @Test
    public void testeMetodoPesquisarBeneficioPorIdPositivo(){
        Beneficio beneficio = new Beneficio();
        Optional<Beneficio> opitionalBeneficio = Optional.of(beneficio);
        Mockito.when(beneficioRepository.findById(Mockito.anyInt())).thenReturn(opitionalBeneficio);
        Beneficio test = beneficioService.pesquisarBeneficioPorID(1);
        Assertions.assertEquals(test, beneficio);
    }

    @Test
    public void testeMetodoPesquisarBeneficioPorIdNegativo(){
        Beneficio beneficio = new Beneficio();
        Optional<Beneficio> optionalBeneficio = Optional.empty();
        Mockito.when(beneficioRepository.findById(Mockito.anyInt())).thenReturn(optionalBeneficio);
        Assertions.assertThrows(RuntimeException.class, () -> {
            beneficioService.pesquisarBeneficioPorID(1);
        });
    }

}
