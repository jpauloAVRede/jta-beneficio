package br.com.zup.JTABeneficio.beneficio;

import br.com.zup.JTABeneficio.beneficio.dtos.entrada.CriarBeneficioDTO;
import br.com.zup.JTABeneficio.cartao.CartaoService;
import br.com.zup.JTABeneficio.cartao.dtos.entrada.CadastrarCartaoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(BeneficioController.class)
public class BeneficioControllerTest {

    @MockBean
    private BeneficioService beneficioService;

    @Autowired
    private MockMvc mockMvc;

    private CriarBeneficioDTO beneficioDTO;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void SetUp (){
        beneficioDTO.setNumeroBeneficio("12");
        beneficioDTO.setTipoBeneficio("VA");
        beneficioDTO.setSaldo(500.00);


        objectMapper = new ObjectMapper();
    }

}
