package br.com.zup.JTABeneficio.estabelecimento;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class EstabelecimentoServiceTest {

    @Autowired
    private EstabelecimentoService estabelecimentoService;
    @MockBean
    private EstabelecimentoRepository estabelecimentoRepository;

    @Test
    public void testeMetodoCadastrarEstabelecimentp(){

        Estabelecimento estabelecimento = new Estabelecimento();

        this.estabelecimentoService.cadastrarEstabelecimento(estabelecimento);
        Mockito.verify(estabelecimentoRepository, Mockito.times(1)).save(estabelecimento);
    }

    @Test
    public void testeMetodoPesquisarEstabelecimentoPorCNPJPositivo(){
        Estabelecimento estabelecimento = new Estabelecimento();
        Optional<Estabelecimento> opitionalEstablecimento = Optional.of(estabelecimento);
        Mockito.when(estabelecimentoRepository.findById(Mockito.anyString())).thenReturn(opitionalEstablecimento);
        Estabelecimento test = estabelecimentoService.pesquisaEstabelecimentoPorCNPJ("01.145.356/0001-86");
        Assertions.assertEquals(test, estabelecimento);
    }

    @Test
    public void testeMetodoPesquisarEstabelecimentoPorCNPJNegativo(){
        Estabelecimento estabelecimento = new Estabelecimento();
        Optional<Estabelecimento> optionalEstabelecimento = Optional.empty();
        Mockito.when(estabelecimentoRepository.findById(Mockito.anyString())).thenReturn(optionalEstabelecimento);
        Assertions.assertThrows(RuntimeException.class, () -> {
            estabelecimentoService.pesquisaEstabelecimentoPorCNPJ("01.145.356/0001-86");
        });
    }

}
