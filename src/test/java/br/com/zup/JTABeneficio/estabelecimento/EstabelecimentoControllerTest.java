package br.com.zup.JTABeneficio.estabelecimento;

import br.com.zup.JTABeneficio.estabelecimento.dtos.entrada.CadastrarEstabelecimentoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(EstabelecimentoController.class)
public class EstabelecimentoControllerTest {

        @MockBean
        private EstabelecimentoService estabelecimentoService;

        @Autowired
        private MockMvc mockMvc;

        private CadastrarEstabelecimentoDTO estabelecimentoDTO;
        private ObjectMapper objectMapper;

        @BeforeEach
        public void SetUp (){
            estabelecimentoDTO.setCnpj("23498754632");
            estabelecimentoDTO.setNome("SupermercadoReal");
            //estabelecimentoDTO.setTipo("Alimenticio");
            estabelecimentoDTO.setGerarCashBack(true);

            objectMapper = new ObjectMapper();
        }
}
