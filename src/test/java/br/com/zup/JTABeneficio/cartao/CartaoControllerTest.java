package br.com.zup.JTABeneficio.cartao;

import br.com.zup.JTABeneficio.cartao.dtos.entrada.CadastrarCartaoDTO;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

@WebMvcTest(CartaoController.class)
public class CartaoControllerTest {

    @MockBean
    private CartaoService cartaoService;

    @Autowired
    private MockMvc mockMvc;

    private CadastrarCartaoDTO cartaoDTO;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void SetUp (){
        cartaoDTO.setBeneficio(12);
        cartaoDTO.setCodSeguranca(654);
        cartaoDTO.setDataExpiracao(LocalDate.now());
        cartaoDTO.setNumero("1");

        objectMapper = new ObjectMapper();
    }
}
