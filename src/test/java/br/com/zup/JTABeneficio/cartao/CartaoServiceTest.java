package br.com.zup.JTABeneficio.cartao;

import br.com.zup.JTABeneficio.beneficio.Beneficio;
import br.com.zup.JTABeneficio.beneficio.BeneficioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootTest
public class CartaoServiceTest {
    @Autowired
    private CartaoService cartaoService;

    @MockBean
    private CartaoRepository cartaoRepository;
    @MockBean
    private BeneficioService beneficioService;

    private Cartao cartaoTeste;
    private Beneficio beneficio;

    @BeforeEach
    private void setup(){
        this.beneficio = new Beneficio();

        this.cartaoTeste = new Cartao();
        cartaoTeste.setNumero("5198820175353293");
        cartaoTeste.setCodSeguranca(777);
        cartaoTeste.setDataExpiracao(LocalDate.now());
        cartaoTeste.setBeneficio(beneficio);

    }

    @Test
    public void testeMetodoCadastrarCartao(){
        Mockito.when(this.beneficioService.pesquisarBeneficioPorID(Mockito.anyInt())).thenReturn(beneficio);
        cartaoTeste.setBeneficio(beneficio);
        this.cartaoService.cadastrarCartao(cartaoTeste, beneficio.getId());
        Mockito.verify(this.cartaoRepository, Mockito.times(1)).save(cartaoTeste);
    }

    @Test
    public void testeMetodoVerificaCartaoDuplicadoCaminhoPositivo(){
        Mockito.when(this.cartaoRepository.existsById(cartaoTeste.getNumero())).thenReturn(false);
    }

    @Test
    public void testeMetodoVerificaCartaoDuplicadoCaminhoNegativo(){
        Mockito.when(this.cartaoRepository.existsById(cartaoTeste.getNumero())).thenReturn(true);
        RuntimeException exception = Assertions
                .assertThrows(RuntimeException.class, () -> {this.cartaoService.verificaCartaoDuplicado(cartaoTeste.getNumero());});
        Assertions.assertFalse(exception.getMessage().equals("Cartão já cadastrado"));

    }

    @Test
    public void testeMetodoPesquisarCartãoPeloNumeroPositivo(){
        Cartao cartao = new Cartao();
        Optional<Cartao> opitionalCartao = Optional.of(cartao);
        Mockito.when(cartaoRepository.findById(Mockito.anyString())).thenReturn(opitionalCartao);
        Cartao test = cartaoService.pesquisarCartaoPeloNumero("7654328926448762");
        Assertions.assertEquals(test, cartao);
    }

    @Test
    public void testeMetodoPesquisarCartãoPeloNumeroNegativo(){
        Cartao cartao = new Cartao();
        Optional<Cartao> optionalCartao = Optional.empty();
        Mockito.when(cartaoRepository.findById(Mockito.anyString())).thenReturn(optionalCartao);
        Assertions.assertThrows(RuntimeException.class, () -> {
            cartaoService.pesquisarCartaoPeloNumero("7654328926448762");
        });
    }

}
